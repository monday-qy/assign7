
import java.io.*;
import java.util.*;


public class Histagram
{
  int[] numbers = null;
  int size = -1;

  public Histagram()
  {
    numbers = null;
    size = -1;
  }

  public int countNumbersInFile(String fileName) throws IOException
  {
    Scanner fileScanner = new Scanner(new File(fileName));
    this.size = 0;

    while (fileScanner.hasNextLine())
    {
      int iValue = fileScanner.nextInt();
      size++;
    }

    fileScanner.close();

    return size;
  }

  public void readFromFileToArray(String fileName) throws IOException
  {
    Scanner fileScanner = new Scanner(new File(fileName));
    this.numbers = new int[this.size];

    int index = 0;
    while (fileScanner.hasNextLine())
    {
      this.numbers[index] = fileScanner.nextInt();
      index++;
    }
    fileScanner.close();
    
  }

  
  public String showHistogram()
  {
    
     String str = " ";
     String str1 = " ", str2 = " ", str3 = " ", str4 = " ";
     String str5 = " ", str6 = " ", str7 = " ", str8 = " ";
     String str9 = " ", str10 = " ";
    int min1 = 1, min11 = 11, min21 = 21, min31 = 31;
    int min41 = 41, min51 = 51, min61 = 61, min71 =71;
    int min81 = 81, min91 =91;
    int max10 = 10, max20 = 20, max30 = 30, max40 = 40;
    int max50 = 50, max60 = 60, max70 = 70, max80 = 80;
    int max90 = 90, max100 = 100;
    int stars1 = 0, stars2 = 0, stars3 = 0, stars4 = 0, stars5 = 0;
    int stars6 = 0, stars7 = 0, stars8 = 0, stars9 = 0, stars10 = 0;
    int value = 0;
    

    
    for (int index = 0; index < numbers.length; index++)     
    {
      value = numbers[index];
      if(value >=min1 && value <= max10)
        stars1++;
      else if(value >= min11 && value <= max20 )
        stars2++;
      else if(value >= min21 && value <= max30 )
        stars3++;
      else if(value >= min31 && value <= max40 )
        stars4++;
      else if(value >= min41 && value <= max50 )
        stars5++;
      else if(value >= min51 && value <= max60 )
        stars6++;
      else if(value >= min61 && value <= max70 )
        stars7++;
      else if(value >= min71 && value <= max80 )
        stars8++;
      else if(value >= min81 && value <= max90 )
        stars9++;
      else if(value >= min91 && value <= max100 )
        stars10++;
      
    }
    int [] star = { stars1, stars2, stars3, stars4, stars5,
                    stars6, stars7, stars8, stars9, stars10};
    double d = 60.00;
    Arrays.sort(star);
    double Max = star[9];
    double STAR_VALUE = Max / d;
    
    System.out.println("One * is roughly equal to " + STAR_VALUE +" values");
    System.out.println();
    System.out.println();
    
    
    for(double index = (stars1 / STAR_VALUE)  ; index > 0; index--)
    {
      str1 += "*";      
    }
    
    System.out.println(" 1 to 10 " + "\t" + str1);
    
    for(double index = stars2 / STAR_VALUE ; index > 0; index--)
    {
      str2 += "*"; 
    }
    System.out.println("11 to 20 " + "\t" + str2);
    
    for(double index = stars3 / STAR_VALUE; index > 0; index--)
    {
      str3 += "*"; 
    }
    System.out.println("21 to 30 " + "\t" + str3);
    
    for(double index = stars4 / STAR_VALUE; index > 0; index--)
    {
      str4 += "*"; 
    }
    System.out.println("31 to 40 " + "\t" + str4);
    for(double index = stars5 / STAR_VALUE; index > 0; index--)
    {
      str5 += "*"; 
    }
    System.out.println("41 to 50 " + "\t" + str5);
    for(double index = stars6 / STAR_VALUE; index > 0; index--)
    {
      str6 += "*"; 
    }
    System.out.println("51 to 60 " + "\t" + str6);
    for(double index = stars7 / STAR_VALUE; index > 0; index--)
    {
      str7 += "*"; 
    }
    System.out.println("61 to 70 " + "\t" + str7);
    for(double index = stars8 / STAR_VALUE; index > 0; index--)
    {
      str8 += "*"; 
    }
    System.out.println("71 to 80 " + "\t" + str8);
    for(double index = stars9 / STAR_VALUE ; index > 0; index--)
    {
      str9 += "*"; 
    }
    System.out.println("81 to 90 " + "\t" + str9);
    for(double index = stars10  / STAR_VALUE; index > 0; index--)
    {
      str10 += "*"; 
    }
    System.out.println("91 to 100 " + "\t" + str10);
    
    
    return str;
    
  }

  

}
