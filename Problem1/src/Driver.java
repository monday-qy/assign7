//********************************************************************
//File:         Driver.java       
//Author:       YANSHUN QIU
//Date:         Nov. 19th
//Course:       CPS100
//
//Problem Statement:
//Write a program that creates a histogram that allows
//you to visually inspect that frequency distribution
//of a set of values. The program should read in an
//arbitrary number of integers from a text input file
//that are in the range 1 to 100 inclusive; then produce
//a chart similar to the one below that indicates how 
//many input values fell in the range 1 to 10, 11 to 20, 
//and so on. Print one asterisk for each value entered. 

//
//Inputs:    
//Outputs:   
//********************************************************************

import java.io.*;

public class Driver
{

  public static void main(String[] args) throws IOException
  {
    final String FILENAME = "realdata.txt";
    Histagram hs = new Histagram();
    
    hs.countNumbersInFile(FILENAME);
    
    hs.readFromFileToArray(FILENAME);

 
    System.out.println(hs.showHistogram());
   
  }

}